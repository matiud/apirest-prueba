<?php
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


//ruta para validar al usuario
$router->post('/users/login','UsersController@getToken');
$router->post('/users','UsersController@createUser');

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function(){
    return Str::random(32);
});

//Esto hace que el usuaro solo acceda si esta autenticado
$router->group(['middleware' => 'auth'], function () use($router) {
    $router->get('/users','UsersController@index');
    $router->put('/users','UsersController@edit');
    $router->delete('/users','UsersController@delete');
});

