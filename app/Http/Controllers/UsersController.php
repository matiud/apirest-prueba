<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(Request $request){

       //Solo si es de tipo appicationjson se devuelve. Es decir solo de una app no de un navegador
       if($request->isJson()){
         // Eloquent para activarlos hay que descomentar una linea de codigo($app->withEloquent();) ubicada en ./bootstrap/app.php
        $users= User::all();
        return response()->json([$users],200);
       }
        return response()->json(['error'=>'Unautorized'],401,[]);
    }

    public function createUser(Request $request){
        if($request->isJson()){
                //Eloquent tiene un facadas(necesitan ser activadas) create

                $user = new User;
                $user->name=$request->name;
                $user->username=$request->username;
                $user->email=$request->email;
                $user->password=Hash::make($request->password);
                $user->api_token=Str::random(60);

                $user->save();
          
             return response()->json($user,201);
          }
           return response()->json(['error'=>'Unautorized'],401,[]);
    }

    public function edit(Request $request){
        if($request->isJson()){

            $user = User::where('email',$request->email)->first();
            
            $user->name=$request->name;
            $user->username=$request->username;

            $user->save();

            return response()->json($user,201);

        }return response()->json(['error'=>'Unautorizes'],401,[]);
    }

    public function delete(Request $request){
        if($request->isJson()){

            $user = User::where('email',$request->email)->first();

            $user->delete();

            return response()->json(['Success  delete ' => $user],201);
        }return response()->json(['error'=>'Unautorizes'],401,[]);
    }
    

public function getToken(Request $request){
    if($request->isJson()){
        try{ 
            $user = User::where('username',$request->username)->first();

            if($user && Hash::check($request->password,$user->password)){
                return response()->json($user,200);
             }else{
               return response()->json(['error'=>'No Content'],406);
               }
         }catch(ModelNotFoundException $e){
            return response()->json(['error'=>'No content'],406);
            }
           
     }
     return response()->json(['error'=>'Unautorized'],401,[]);
    
    }


    
    //
}
